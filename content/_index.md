### Participate to a retreat among students, researchers and experts during 2 full days in Switzerland to discuss, debate, and write a research agenda.

**Place** : *Cornol, in the beautiful swiss Jura* 

**Dates**: 10-13 November, 2021

The main goal is to formalize developing practices that remain invisible or difficult to access while developing 
critical thinking. A secondary goal is bringing women together from different disciplines that are estranged by different reasons but bounded 
by a specific preoccupation: how to deconstruct algorithmic systems now shaping our daily lives.  

The retreat will help creating a network and acquiring conceptual and analytical skills for understanding algorithmic practices. We will ultimately offer a new thinking perspective by women, 
for society to introduce diversity whereas the male gaze is predominant in questions women are also directly affected by.

This event is an opportunity to share knowledge, set the tracks for new research venues and raise awareness 
about developing practices producing biases.
