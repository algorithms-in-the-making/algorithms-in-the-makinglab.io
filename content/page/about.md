---
title: About
subtitle: 
comments: false
---

Artificial intelligence and decision support algorithms have gained attention as an entity on its own thanks 
to the potential performance they offer in terms of data collection and handling. 
It has also been demonstrated that they reproduce and amplify biases in online dating platforms, social media, 
translation tools and image processing. For better or worse, the *making* of algorithms rely on human 
processes that are still unexplored.

Indeed, algorithms reflect social stereotypes and could potentially reinforce, or transform them. 
Biases are contained in the input data, the labelling, as manual treatment is still highly required, the user samples,
the experimental methodology... These are all beforehand practices reaching developers, engineers, data analysts, 
human and social scientists in a context where more and more data is digital-native. 
The major preoccupation underlying is that the ad hoc results obtained by these entities are widespread as a 
support for decision making in academia, private companies and the public sector without fully 
understanding how they came to be.
 
In order to remove heteronormative stereotypes, minorities discrimination or glass 
ceiling effects in the online platforms that are daily used, you are invited to this student retreat for master, PhD students and young researchers (women or non-binary) in Switzerland to discuss, debate, and 
write a research agenda while acquiring skills and knowledge. The research agenda will provide a cursory sketch of algorithmic biases *in the making* and how we 
think multidisciplinary research will look like in the future. It is not so much meant as a forecast, 
but as a proposal - a stimulus for further research and call for action. For instance, the research agenda 
could present case studies producing gender logics, unfold the programming practices to identify critical 
steps for biases reproduction, and introduce corrective measures with success cases. 
The main goal is to formalize developing practices that remain invisible or difficult to access while 
developing critical thinking. A secondary goal is bringing together students and young researchers that are estranged by different 
reasons but bounded by a specific preoccupation. This will help creating a network of a new thinking 
perspective *by* women, *for* society to introduce diversity whereas the male gaze is predominant in questions 
women are also directly affected by.

## Organization and Acknowledgements

- Organized by [Dr. Jessica Pidoux](https://www.linkedin.com/in/jessicapidoux/?locale=en_US)
- Funded by [Women in big data](https://www.wibd.ch/retreat)  within the framework of NRP 75-Big Data National Research Programme and the Digital Society Initiative, University of Zurich
- Supported by [Digital Humanities Institute](https://www.epfl.ch/schools/cdh/research-2/dhi/), [Women@thetable](https://www.womenatthetable.net/) and the student and researcher association dhelta UNIL-EPFL 
- Website design thanks to Sofia Ares
- Academic support by Prof. Daniel Gatica-Perez and Dr. Charlotte Mazel-Cabasse from the [dhcenter UNIL-EPFL](https://dhcenter-unil-epfl.com/) 
- Special support for doctoral students given by the University of Lausanne, [PDEN](https://www.unil.ch/doc-digitalstudies/fr/home.html)
