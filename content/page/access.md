## Location

The retreat will take place in the jura BIO farmhouse Le Pécal, with a guesthouse.
We will be staying in the guesthouse, which has dormitories so we'll be sharing rooms with 2 or three persons. No private rooms available.   
Visit [Le Pécal website](https://fermelepecal.wixsite.com/fermelepecal) for more details.

Route d'Alle 1  
2952 Cornol  
Switzerland  

## What to bring
Casual and warm clothes, comfortable shoes for walking, towels will be provided.

## How to get there

**From Zürich, Basel, Geneva and Lausanne**:   
Take a train to Courgenay, 
(from some destinations you will have to switch train at Biel/Bienne), and then take the bus at the stop Courgenay, 
Petite Gilberte Bus 76 7637 Direction Charmoille, douane until Cornol, bas du village.   
The farm will be 448 m away from the stop by foot.

## Access Map

https://goo.gl/maps/Uy8k48T3JbPnGfpFA

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d18163.682805629105!2d7.172259939250183!3d47.41101264313211!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4791f0f7713f8bf7%3A0x7f85f9056f4cf467!2sRoute%20d&#39;Alle%201%2C%202952%20Cornol!5e0!3m2!1sfr!2sch!4v1579698484113!5m2!1sfr!2sch" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
