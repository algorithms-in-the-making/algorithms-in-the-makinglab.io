# Registration

In order to register, please fill in this form: https://framaforms.org/algorithms-in-the-making-registration-1634053579   
_The following personal data are collected in the form: name, last name, email, occupation and affiliation. This information will only be used for the retreat organization: hotel reservation, to contact you if necessary, and it will appear in the final reports written. It is stored by framaform and the form will be deleted after sending the acceptance confirmation on October 30, 2021._

The number of participants is restricted to 10 students and young researchers (women and non-binary).
The costs covered include accommodation for three nights for all the participants and food.
Transportation costs are not covered. Participants must cover their transportation fees.

We are still looking for some funding to cover the food and drinks for everybody.
If we do not manage to find additional financial resources, we might ask you to contribute with lunch or dinner (free price according to your financial situation).

### Contact
If you have questions, please contact the organizer at jessica.pidoux@sciencespo.fr

### Timeline for registration
Registration is open until the limit (10 participants) is reached. 

### Who can participate ?

**Participants who identify as women, or are non-binary**

Master, PhD students and post-doctoral researchers coming from the human and social sciences, or computer science, that together cover a 
_diversity of training, stage of research or project advancement, cultural background_
and that are available for the 2 full days of workshop, 3 nights.

Whether you have experience in algorithmic studies and algorithm's development or not is not an issue. In this retreat you will learn more about it and share your experience. References are mantadory to read so we all have a common background.

**The number of participants is limited to 10.**   
We cannot guarantee a place above this limit.

If you do not cover all the criteria but think your participation would still be of interest 
for the whole group, please register and we will carefully examine your submission.

### Health measures
The health measures of the Swiss confederation to be respected are the following:
- Vaccination against COVID-19, cure or negative result of a PCR or rapid antigen test to be presented at your arrival.
More info on the certificates [here](https://www.vd.ch/toutes-les-actualites/hotline-et-informations-sur-le-coronavirus/faq-covid-et-sante/certificat-covid/)
